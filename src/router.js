import Vue from "vue";
import Router from "vue-router";
import DashboardLayout from "@/layout/DashboardLayout";
import AuthLayout from "@/layout/AuthLayout";

import AddSiswa from "./views/Tables/AddSiswa";
import AddAyah from "./views/Tables/AddAyah";
import AddIbu from "./views/Tables/AddIbu";
import AddWali from "./views/Tables/AddWali";
import AddPriodik from "./views/Tables/AddPriodik";
import AddKeluar from "./views/Tables/AddKeluar";
import AddPrestasi from "./views/Tables/AddPrestasi";
import AddBeasiswa from "./views/Tables/AddBeasiswa";

import AyahAssignAnak from "./views/Tables/AyahAssignAnak";
import IbuAssignAnak from "./views/Tables/IbuAssignAnak";
import WaliAssignAnak from "./views/Tables/WaliAssignAnak";

import UpdateSiswa from "./views/Tables/UpdateSiswa";
import UpdateAyah from "./views/Tables/UpdateAyah";
import UpdateIbu from "./views/Tables/UpdateIbu";
import UpdateWali from "./views/Tables/UpdateWali";
import UpdatePrestasi from "./views/Tables/UpdatePrestasi";
import UpdateBeasiswa from "./views/Tables/UpdateBeasiswa";

import UpdatePriodik from "./views/Tables/UpdatePriodik";

Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      redirect: "dashboard",
      component: DashboardLayout,
      children: [
        {
          path: "/dashboard",
          name: "dashboard",
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () =>
            import(/* webpackChunkName: "demo" */ "./views/Dashboard.vue"),
        },
        {
          path: "/icons",
          name: "icons",
          component: () =>
            import(/* webpackChunkName: "demo" */ "./views/Icons.vue"),
        },
        {
          path: "/profile",
          name: "profile",
          component: () =>
            import(/* webpackChunkName: "demo" */ "./views/UserProfile.vue"),
        },
        {
          path: "/keluar",
          name: "keluar",
          component: () =>
            import(/* webpackChunkName: "demo" */ "./views/Keluar.vue"),
        },

        {
          path: "/orangtua",
          name: "orangtua",
          component: () =>
            import(/* webpackChunkName: "demo" */ "./views/Orangtua.vue"),
        },
        {
          path: "/tables",
          name: "tables",
          component: () =>
            import(/* webpackChunkName: "demo" */ "./views/Tables.vue"),
        },
      ],
    },

    //
    {
      path: "/addsiswa",
      name: "add-siswa",
      component: AddSiswa,
    },
    {
      path: "/addayah",
      name: "add-ayah",
      component: AddAyah,
    },
    {
      path: "/addibu",
      name: "add-ibu",
      component: AddIbu,
    },
    {
      path: "/addwali",
      name: "add-wali",
      component: AddWali,
    },
    {
      path: "/addpriodik",
      name: "add-priodik",
      component: AddPriodik,
    },

    {
      path: "/addkeluar",
      name: "add-keluar",
      component: AddKeluar,
    },

    {
      path: "/addprestasi",
      name: "add-prestasi",
      component: AddPrestasi,
    },

    {
      path: "/addbeasiswa",
      name: "add-beasiswa",
      component: AddBeasiswa,
    },

    {
      path: "/updatesiswa/:id",
      name: "update-siswa",
      component: UpdateSiswa,
    },

    {
      path: "/updateayah/:id",
      name: "update-ayah",
      component: UpdateAyah,
    },

    {
      path: "/updateibu/:id",
      name: "update-ibu",
      component: UpdateIbu,
    },

    {
      path: "/updatewali/:id",
      name: "update-wali",
      component: UpdateWali,
    },

    {
      path: "/updatepriodik/:id",
      name: "update-priodik",
      component: UpdatePriodik,
    },

    {
      path: "/updateprestasi/:id",
      name: "update-prestasi",
      component: UpdatePrestasi,
    },

    {
      path: "/updatebeasiswa/:id",
      name: "update-beasiswa",
      component: UpdateBeasiswa,
    },

    {
      path: "/ayahassignanak/:id",
      name: "ayah-assignanak",
      component: AyahAssignAnak,
    },

    {
      path: "/ibuassignanak/:id",
      name: "ibu-assignanak",
      component: IbuAssignAnak,
    },

    {
      path: "/waliassignanak/:id",
      name: "wali-assignanak",
      component: WaliAssignAnak,
    },

    //

    {
      path: "/",
      redirect: "login",
      component: AuthLayout,
      children: [
        {
          path: "/login",
          name: "login",
          component: () =>
            import(/* webpackChunkName: "demo" */ "./views/Login.vue"),
        },
        {
          path: "/register",
          name: "register",
          component: () =>
            import(/* webpackChunkName: "demo" */ "./views/Register.vue"),
        },
      ],
    },
  ],
});
